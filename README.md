selinux
=======

Install and configure selinux on a debian system.

TODO
----

Proper mcstransd support.

Requirements
------------

A debian system.

Role Variables
--------------

See `defaults/main.yml`

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: selinux, tags: [ "selinux" ] }

License
-------

GPL-3.0

Author Information
------------------

bauen1
