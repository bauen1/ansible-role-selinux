---
- name: fetch gpg keys used to verify policy integrity
  gpg_import:
    key_id: "{{ item }}"
    state: "latest"
  with_items: "{{ selinux_gpg_whitelist }}"
  when: selinux_verify_commit
  tags:
    - selinux-policy-update

# we set this as fact, so even if become is true
# the selinux_policy_src_fact still points to the policy
# wherever that might be
- name: set selinux_policy_src_fact
  set_fact:
    selinux_policy_src_fact: "{{ selinux_policy_src }}"
    cacheable: true
  tags:
    - always

- name: "clone {{ selinux_policy_repo }} to {{ selinux_policy_src_fact }}"
  git:
    dest: "{{ selinux_policy_src_fact }}"
    repo: "{{ selinux_policy_repo }}"
    recursive: true
    version: "{{ selinux_policy_version }}"
  tags:
    - selinux-git-pull
    - selinux-policy-update
  register: git_checkout

# verify the git commit
# XXX: this has to be seperate from the above for some reason
- name: verify git commit signature
  git:
    dest: "{{ selinux_policy_src_fact }}"
    repo: "{{ selinux_policy_repo }}"
    clone: false
    update: false
    version: "{{ selinux_policy_version }}"
    verify_commit: "{{ selinux_verify_commit }}"
    gpg_whitelist: "{{ selinux_gpg_whitelist }}"
  tags:
    - selinux-policy-update

#
- name: make conf
  make:
    chdir: "{{ selinux_policy_src_fact }}"
    params: "{{ selinux_make_params }}"
    target: "conf"
  when: (git_checkout is not defined) or (git_checkout is changed)
  tags:
    - selinux-policy-update

# we need to enable this boolean otherwise ansible itself will
# be prohibited by selinux from becoming root
- name: enable ssh_sysadm_login
  lineinfile:
    path: "{{ selinux_policy_src_fact }}/policy/booleans.conf"
    line: "ssh_sysadm_login = true"
    regexp: "ssh_sysadm_login = false"
    state: present

# build the policy
- name: "make base"
  make:
    chdir: "{{ selinux_policy_src_fact }}"
    params: "{{ selinux_make_params }}"
    target: "base"
  tags:
    - selinux-policy-update
  register: policy_build

# install the policy
- name: "make install"
  become: true
  make:
    chdir: "{{ selinux_policy_src_fact }}"
    params: "{{ selinux_make_params }}"
    target: "install"
  tags:
    - selinux-policy-update
  register: policy_install

# this target will always reinstall everything, so we only execute it
# if really necessary
- name: "make install-headers"
  become: true
  make:
    chdir: "{{ selinux_policy_src_fact }}"
    params: "{{ selinux_make_params }}"
    target: "install-headers"
  tags:
    - selinux-policy-update
  when: (policy_build is changed) or (policy_install is changed)
  register: policy_install_headers
